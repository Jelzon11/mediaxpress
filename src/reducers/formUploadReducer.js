import { FETCH_DATA, ADD_DATA, ADD_NON_EXISTING_MATERIAL } from '../actions/types';

const initialState = {
    items: [],
    item: {},
    nonExistingMaterials: [],

}
export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_DATA:
            return {
                ...state,
                items: action.payload
            }
        case ADD_DATA:
            return {
                ...state,
                items: [...state.items, action.payload]
            };
            case ADD_NON_EXISTING_MATERIAL:
                return {
                    ...state,
                    nonExistingMaterials: [...state.nonExistingMaterials, action.payload]
                };
        default:
            return state;
    }
}