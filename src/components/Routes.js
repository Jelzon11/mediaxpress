import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { connect } from "react-redux";
import DashboardPage from './pages/DashboardPage';
import MediaArchive from './pages/MediaArchive';
import Users from './pages/Users';
import FormToUpload from './pages/FormToUpload';
import NotFoundPage from './pages/NotFoundPage';
import TopNavigation from '../components/topNavigation';
import Footer from '../components/Footer';
import { fetchMaterial } from "../actions/formUploadAction";
import NonExistingMaterial from './pages/NonExistingMaterial'

const Routes = (props) => {
  //props.fetchMaterial();
  
    return (
      <div className="flexible-content">
      <TopNavigation />
      <main id="content" className="p-5">
      <Switch>
        <Route path='/' exact component={DashboardPage} />
        <Route path='/dashboard' component={DashboardPage} />
        <Route path='/mediaArchive' component={MediaArchive} />
        <Route path='/users' component={Users} />
        <Route path='/uploadData' component={FormToUpload} />
        <Route path='/nonExisting' component={NonExistingMaterial} />
        <Route component={NotFoundPage} />
      </Switch>
      </main>
      <Footer />
      </div>
    );
  }

  export default connect(null, {
    //fetchMaterial
  })(Routes);
