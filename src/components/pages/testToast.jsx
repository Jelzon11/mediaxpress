import React from 'react'
import { ToastProvider, useToasts } from 'react-toast-notifications'

const FormWithToasts = () => {
  const { addToast } = useToasts()

  const onSubmit = (e) => {
      e.preventDefault();
      addToast('Saved Successfully', { 
        appearance: 'success',
        autoDismiss: true 
    })
  }
  return <form onSubmit={onSubmit}>
      <button type="submit">Toast</button>
  </form>
}

export default FormWithToasts;