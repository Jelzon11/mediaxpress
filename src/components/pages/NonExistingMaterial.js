import React, { useState, Fragment } from "react";
import Dropzone from "react-dropzone";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import "../../../node_modules/toastr/build/toastr.min.js";
import "../../../node_modules/toastr/build/toastr.css";
import {
  MDBCol,
  MDBRow,
  MDBCard,
  MDBCardBody,
  MDBCardText,
  MDBContainer,
  MDBCardHeader,
  MDBCardFooter,
  MDBBtn
} from "mdbreact";
import { ProgressBar } from "../Utility-Component";
import { setToInvalid, setToValid } from "../utils";
import { uploadNonExistingMaterial } from "../../actions/nonExistingMaterialAction";
import toastr from "toastr";
const audioMaxSize = 100000000; //byte
const acceptedFileTypes = "audio/mp3";
const acceptedFileTypesArray = acceptedFileTypes.split(",").map(item => {
  return item.trim();
});

const NonExistingMaterial = props => {
  const [material, setMaterial] = useState("");
  const [src, setSrc] = useState(null);
  const [duration, setDuration] = useState("");
  const [startRecordTimeStamp, setStartRecordTimeStamp] = useState("");
  const [channel, setChannel] = useState("");
  const [city, setCity] = useState("");
  const [region, setRegion] = useState("");
  const [company, setCompany] = useState("");
  const [brand, setBrand] = useState("");
  const [product, setProduct] = useState("");
  const [mainCategory, setMainCategory] = useState("");
  const [subCategory, setSubCategory] = useState("");
  const [progressPercent, setProgressPercent] = useState(0);
  const [isInProgress, setInProgress] = useState(false);

  //File Verification
  const verifyFile = files => {
    if (files && files.length > 0) {
      const currentFile = files[0];
      const currentFileType = currentFile.type;
      const currentFileSize = currentFile.size;
      if (currentFileSize > audioMaxSize) {
        alert(
          "This file is not allowed." + currentFileSize + " bytes is too large"
        );
        return false;
      }
      if (!acceptedFileTypesArray.includes(currentFileType)) {
        alert("This file is not allowed. Only images/videos are allowed.");
        return false;
      }
      const fileType = currentFile.name.split(".").pop();
      return fileType;
    }
  };
  //File OnDrop event Handler
  const handleOnDrop = (files, rejectedFiles) => {
    if (rejectedFiles && rejectedFiles.length > 0) verifyFile(rejectedFiles);
    if (files && files.length > 0) {
      const isVerified = verifyFile(files);
      if (isVerified) {
        const currentFile = files[0];
        const myFileItemReader = new FileReader();
        myFileItemReader.addEventListener(
          "load",
          () => {
            setSrc(myFileItemReader.result);
            setMaterial(currentFile);
          },
          false
        );
        myFileItemReader.readAsDataURL(currentFile);
      }
    }
  };
  const companyOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const company = e.target.value;
    setCompany(company);
  };
  const brandOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const brand = e.target.value;
    setBrand(brand);
  };
  const productOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const product = e.target.value;
    setProduct(product);
  };
  const mainCategoryOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const mainCategory = e.target.value;
    setMainCategory(mainCategory);
  };
  const subCategoryOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const subCategory = e.target.value;
    setSubCategory(subCategory);
  };
  const durationOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const duration = e.target.value;
    setDuration(duration);
  };
  const startRecordTimeStampOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const startRecordTimeStamp = e.target.value;
    setStartRecordTimeStamp(startRecordTimeStamp);
  };
  const channelOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const channel = e.target.value;
    setChannel(channel);
  };
  const cityOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const city = e.target.value;
    setCity(city);
  };
  const regionOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const region = e.target.value;
    setRegion(region);
  };

  const handleSubmit = e => {
    e.preventDefault();
    if (!duration) setToInvalid(e.target.elements["duration"]);
    if (!startRecordTimeStamp)
      setToInvalid(e.target.elements["startRecordTimeStamp"]);
    if (!channel) setToInvalid(e.target.elements["channel"]);
    if (!city) setToInvalid(e.target.elements["city"]);
    if (!region) setToInvalid(e.target.elements["region"]);
    if (!material) setToInvalid(e.target.elements["material"]);
    if (!company) setToInvalid(e.target.elements["company"]);
    if (!brand) setToInvalid(e.target.elements["brand"]);
    if (!product) setToInvalid(e.target.elements["product"]);
    if (!mainCategory) setToInvalid(e.target.elements["mainCategory"]);
    if (!subCategory) setToInvalid(e.target.elements["subCategory"]);
    if (
      !duration ||
      !startRecordTimeStamp ||
      !channel ||
      !city ||
      !region ||
      !material ||
      !company ||
      !brand ||
      !product ||
      !mainCategory ||
      !subCategory
    )
      return false;
    let formData = new FormData();
    formData.set("duration", duration);
    formData.set("startRecordTimestamp", startRecordTimeStamp);
    formData.set("channel", channel);
    formData.set("city", city);
    formData.set("region", region);
    formData.set("companyName", company);
    formData.set("brandName", brand);
    formData.set("productName", product);
    formData.set("file", material);
    formData.set("mainCategory", mainCategory);
    formData.set("subCategory", subCategory);
    console.log("formdata " + formData);
    setInProgress(true);
    props.uploadNonExistingMaterial(
      formData,
      percent => setProgressPercent(percent),
      () => {
        toastr.success("Data Successfully Uploaded", "System");
        handleResetForm();
      }
    );
    toastr.success("all is well, Rancho");
  };
  const handleResetForm = () => {
    setDuration("");
    setStartRecordTimeStamp("");
    setChannel("");
    setRegion("");
    setCity("");
    setSrc(null);
    setMaterial(null);
    setInProgress(false);
    setCompany("");
    setBrand("");
    setProduct("");
    setMainCategory("");
    setSubCategory("");
  };
  return (
    <React.Fragment>
      <MDBContainer>
        <form
          className="form-group needs-validation"
          onSubmit={handleSubmit}
          noValidate
        >
          <MDBCard>
            <MDBCardHeader>
              <h3 style={{ fontWeight: "bold" }}>Non-Existing Materials</h3>{" "}
              <br />
            </MDBCardHeader>
            <MDBCardBody>
              <MDBRow>
                <MDBCol md="6">
                  <MDBCol>
                    <label>Company Name:</label>
                    <input
                      name={"company"}
                      value={company}
                      type="text"
                      className="form-control"
                      onChange={e => companyOnChangeHandler(e)}
                      required
                    />
                    <div className="invalid-feedback">
                      Please provide company name
                    </div>
                  </MDBCol>
                  <br />
                  <MDBCol>
                    <label>Brand Name:</label>
                    <input
                      name={"brand"}
                      value={brand}
                      type="text"
                      className="form-control"
                      onChange={e => brandOnChangeHandler(e)}
                      required
                    />
                    <div className="invalid-feedback">
                      Please provide brand name
                    </div>
                  </MDBCol>
                  <br />
                  <MDBCol>
                    <label>Product Name:</label>
                    <input
                      name={"product"}
                      value={product}
                      type="text"
                      className="form-control"
                      onChange={e => productOnChangeHandler(e)}
                      required
                    />
                    <div className="invalid-feedback">
                      Please provide product name
                    </div>
                  </MDBCol>
                  <br />
                  <MDBCol>
                    <label>Main Category Name:</label>
                    <input
                      name={"mainCategory"}
                      value={mainCategory}
                      type="text"
                      className="form-control"
                      onChange={e => mainCategoryOnChangeHandler(e)}
                      required
                    />
                    <div className="invalid-feedback">
                      Please provide Main Category name
                    </div>
                  </MDBCol>
                  <br />
                  <MDBCol>
                    <label>Sub Category Name:</label>
                    <input
                      name={"subCategory"}
                      value={subCategory}
                      type="text"
                      className="form-control"
                      onChange={e => subCategoryOnChangeHandler(e)}
                      required
                    />
                    <div className="invalid-feedback">
                      Please provide Sub Category name
                    </div>
                  </MDBCol>
                  <br />
                </MDBCol>
                <MDBCol>
                  <br />
                  <MDBCol>
                    <label>Duration (in secs.)</label>
                    <input
                      name={"duration"}
                      value={duration}
                      type="text"
                      className="form-control"
                      onChange={e => durationOnChangeHandler(e)}
                      required
                    />
                    <div className="invalid-feedback">
                      Please provide duration
                    </div>
                  </MDBCol>
                  <br />
                  <MDBCol>
                    <label>Start Record Timestamp</label>
                    <input
                      name={"startRecordTimeStamp"}
                      value={startRecordTimeStamp}
                      type="text"
                      className="form-control"
                      onChange={e => startRecordTimeStampOnChangeHandler(e)}
                      required
                    />
                    <div className="invalid-feedback">
                      Please provide starting record timestamp
                    </div>
                  </MDBCol>
                  <br />
                  <MDBCol>
                    <label>Channel</label>
                    <input
                      name={"channel"}
                      value={channel}
                      type="text"
                      className="form-control"
                      onChange={e => channelOnChangeHandler(e)}
                      required
                    />
                    <div className="invalid-feedback">
                      Please provide channel
                    </div>
                  </MDBCol>
                  <br />
                  <MDBCol>
                    <label>City</label>
                    <input
                      name={"city"}
                      value={city}
                      type="text"
                      className="form-control"
                      onChange={e => cityOnChangeHandler(e)}
                      required
                    />
                    <div className="invalid-feedback">Please provide City</div>
                  </MDBCol>
                  <br />
                  <MDBCol>
                    <label>Region</label>
                    <input
                      name={"region"}
                      value={region}
                      type="text"
                      className="form-control"
                      onChange={e => regionOnChangeHandler(e)}
                      required
                    />
                    <div className="invalid-feedback">
                      Please provide region
                    </div>
                  </MDBCol>
                  <br />
                  <MDBCol>
                    <label htmlFor="material">Choose File</label>
                    <Dropzone
                      onDrop={handleOnDrop}
                      accept={acceptedFileTypes}
                      multiple={false}
                      maxSize={audioMaxSize}
                    >
                      {({ getRootProps, getInputProps }) => (
                        <Fragment>
                          <div
                            style={{
                              borderColor: "lightgray",
                              borderStyle: "dashed",
                              textAlignment: "center",
                              margin: "5px 15px",
                              height: "120px"
                            }}
                          >
                            <section className={"is-invalid"}>
                              <div
                                {...getRootProps()}
                                style={{ width: "100%" }}
                              >
                                <input
                                  type={"file"}
                                  name={"material"}
                                  key={"material"}
                                  required
                                  {...getInputProps()}
                                />
                                <p
                                  style={{
                                    color: "lightgray",
                                    fontWeight: "bold",
                                    textTransform: "uppercase",
                                    height: "120px",
                                    width: "100%",
                                    textAlign: "center"
                                  }}
                                >
                                  Drag 'n' drop an audio here, or click to
                                  select
                                </p>
                                <div className="invalid-feedback">
                                  Please Attach Audio File
                                </div>
                              </div>
                            </section>
                          </div>
                        </Fragment>
                      )}
                    </Dropzone>
                    <hr />
                    {src && material && (
                      <Fragment>
                        <label>Preview</label>
                        <div style={{ padding: "1rem" }}>
                          <MDBCard>
                            <MDBCardBody>
                              <MDBCardText>
                                Title :{" "}
                                <text style={{ color: "#3283a8" }}>
                                  {material.name}
                                </text>
                              </MDBCardText>
                              <MDBCardText>
                                Type:{" "}
                                <text style={{ color: "#3283a8" }}>
                                  {material.type}
                                </text>
                              </MDBCardText>
                              {src && (
                                <audio
                                  src={src}
                                  controls
                                  style={{ width: "100%" }}
                                ></audio>
                              )}
                            </MDBCardBody>
                          </MDBCard>
                        </div>
                      </Fragment>
                    )}
                  </MDBCol>
                </MDBCol>
              </MDBRow>
            </MDBCardBody>
            <MDBCardFooter style={{ textAlign: "right" }}>
              {isInProgress ? (
                <MDBCol>
                  <label htmlFor="material">Progress</label>
                  <ProgressBar percent={progressPercent} />
                </MDBCol>
              ) : (
                <Fragment>
                  <MDBBtn color="grey" onClick={() => handleResetForm()}>
                    Clear
                  </MDBBtn>
                  <MDBBtn color="light-green" type="submit">
                    Submit
                  </MDBBtn>
                </Fragment>
              )}
            </MDBCardFooter>
          </MDBCard>
        </form>
      </MDBContainer>
      <br />
    </React.Fragment>
  );
};
NonExistingMaterial.propTypes = {
  uploadNonExistingMaterial: PropTypes.func.isRequired
};
export default connect(null, { uploadNonExistingMaterial })(
  NonExistingMaterial
);
