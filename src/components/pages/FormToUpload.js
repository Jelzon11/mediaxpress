import React, { useState, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import Dropzone from "react-dropzone";
import '../../../node_modules/toastr/build/toastr.min.js'
import '../../../node_modules/toastr/build/toastr.css'
import {
  MDBCol,
  MDBRow,
  MDBCard,
  MDBCardBody,
  MDBCardText,
  MDBContainer,
  MDBCardHeader,
  MDBCardFooter,
  MDBBtn
} from "mdbreact";
import { fetchMaterial, uploadMaterial } from "../../actions/formUploadAction";
import { Select, ProgressBar } from "../Utility-Component";
import { violations, setToInvalid, setToValid } from "../utils";
import toastr from 'toastr';
const audioMaxSize = 100000000; //byte
const acceptedFileTypes = "audio/mp3";
const acceptedFileTypesArray = acceptedFileTypes.split(",").map(item => {
  return item.trim();
});

const FormToUpload = props => {
  const [materialId, setMaterialId] = useState("");
  const [referenceId, setReferenceId] = useState("");
  const [title, setTitle] = useState("");
  const [originalDuration, setOriginalDuration] = useState("");
  const [fetchMaterialDetails, setFetchMaterialDetails] = useState({});
  const [violation, selectedViolation] = useState(0);
  const [material, setMaterial] = useState("");
  const [src, setSrc] = useState(null);
  const [duration, setDuration] = useState("");
  const [startRecordTimeStamp, setStartRecordTimeStamp] = useState("");
  const [scoreQuality, setScoreQuality] = useState("");
  const [channel, setChannel] = useState("");
  const [city, setCity] = useState("");
  const [region, setRegion] = useState("");
  //const [dateAired, setDateAired] = useState("");
  const [status, setStatus] = useState(true);
  const [companyId, setCompanyId] = useState("");
  const [brandId, setBrandId] = useState("");
  const [productId, setProductId] = useState("");
  const [mainCategoryId, setMainCategoryId] = useState("");
  const [subCategoryId, setSubCategoryId] = useState("");
  const [company, setCompany] = useState("");
  const [brand, setBrand] = useState("");
  const [product, setProduct] = useState("");
  const [mainCategory, setMainCategory] = useState("");
  const [subCategory, setSubCategory] = useState("");
  const [commercialType, setCommercialType] = useState("");
  const [progressPercent, setProgressPercent] = useState(0);
  const [isInProgress, setInProgress] = useState(false);
  const [reason, setReason] = useState("");
  
  //File Verification
  const verifyFile = files => {
    if (files && files.length > 0) {
      const currentFile = files[0];
      const currentFileType = currentFile.type;
      const currentFileSize = currentFile.size;
      if (currentFileSize > audioMaxSize) {
        alert(
          "This file is not allowed." + currentFileSize + " bytes is too large"
        );
        return false;
      }
      if (!acceptedFileTypesArray.includes(currentFileType)) {
        alert("This file is not allowed. Only images/videos are allowed.");
        return false;
      }
      const fileType = currentFile.name.split(".").pop();
      return fileType;
    }
  };
  //File OnDrop event Handler
  const handleOnDrop = (files, rejectedFiles) => {
    if (rejectedFiles && rejectedFiles.length > 0) verifyFile(rejectedFiles);
    if (files && files.length > 0) {
      const isVerified = verifyFile(files);
      if (isVerified) {
        const currentFile = files[0];
        const myFileItemReader = new FileReader();
        myFileItemReader.addEventListener(
          "load",
          () => {
            setSrc(myFileItemReader.result);
            setMaterial(currentFile);
          },
          false
        );
        myFileItemReader.readAsDataURL(currentFile);
      }
    }
  };
  const handleMaterialId = e => {
    //setMaterialId(e.target.value);
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    //setMaterialId(e.target.value);
    const getMaterialId = e.target.value;
    props.fetchMaterial(
      {
        id: getMaterialId
      },
      res => {
        setFetchMaterialDetails(res);
        setTitle(res.title);
        setOriginalDuration(res.duration);
        setCompanyId(res.productDetails.companyDetails.id);
        setBrandId(res.productDetails.brandDetails.id);
        setProductId(res.productDetails.id);
        setCommercialType(res.commercialType);
        setMainCategoryId(res.productDetails.mainCategoryDetails.id);
        setSubCategoryId(res.productDetails.subCategoryDetails.id);
        setCompany(res.productDetails.companyDetails.name);
        setBrand(res.productDetails.brandDetails.name);
        setProduct(res.productDetails.name);
        setMainCategory(res.productDetails.mainCategoryDetails.name);
        setSubCategory(res.productDetails.subCategoryDetails.name);
        setReferenceId(res.referenceId);
      }
    );
  };
  const materialIdOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const materialID = e.target.value;
    setMaterialId(materialID);
  };
  const violationOnChangeHandler = e => {
    e.target.value !== 0 ? setToValid(e.target) : setToInvalid(e.target);
    const violation = e.target.value;
    selectedViolation(violation);
    //console.log(violation)
    if (violation !== "1") {
      setStatus(false);
    } else {
      setStatus(true);
    }
  };
  const durationOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const duration = e.target.value;
    setDuration(duration);
  };
  const reasonOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const reason = e.target.value;
    setReason(reason);
  };
  const startRecordTimeStampOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const startRecordTimeStamp = e.target.value;
    setStartRecordTimeStamp(startRecordTimeStamp);
  };
  const scoreQualityOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const scoreQuality = e.target.value;
    setScoreQuality(scoreQuality);
  };
  const channelOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const channel = e.target.value;
    setChannel(channel);
  };
  const cityOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const city = e.target.value;
    setCity(city);
  };
  const regionOnChangeHandler = e => {
    e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
    const region = e.target.value;
    setRegion(region);
  };
  // const dateAiredOnChangeHandler = e => {
  //   e.target.value !== "" ? setToValid(e.target) : setToInvalid(e.target);
  //   const dateAired = e.target.value;
  //   setDateAired(dateAired);
  // };
  const handleSubmit = e => {
    e.preventDefault();
    if (!materialId) setToInvalid(e.target.elements["materialId"]);
    if (!duration) setToInvalid(e.target.elements["duration"]);
    if (!startRecordTimeStamp)
      setToInvalid(e.target.elements["startRecordTimeStamp"]);
    if (!scoreQuality) setToInvalid(e.target.elements["scoreQuality"]);
    if (!channel) setToInvalid(e.target.elements["channel"]);
    if (!city) setToInvalid(e.target.elements["city"]);
    if (!region) setToInvalid(e.target.elements["region"]);
    //if (!dateAired) setToInvalid(e.target.elements["dateAired"]);
    //if (!violation) setToInvalid(e.target.elements["violation"]);
    if (!material) setToInvalid(e.target.elements["material"]);
    //if (!status) setToInvalid(e.target.elements["status"]);
    if (
      !materialId ||
      !duration ||
      !startRecordTimeStamp ||
      !scoreQuality ||
      !channel ||
      !city ||
      !region ||
      !material ||
      //!status ||
      !violation ||
      !companyId ||
      !brandId ||
      !productId ||
      !commercialType ||
      !mainCategoryId ||
      !referenceId
    )
      return false;
    let formData = new FormData();
    formData.set("materialId", materialId);
    formData.set("duration", duration);
    formData.set("startRecordTimestamp", startRecordTimeStamp);
    formData.set("scoreQuality", scoreQuality);
    formData.set("channel", channel);
    formData.set("city", city);
    formData.set("region", region);
    formData.set("companyId", companyId);
    formData.set("brandId", brandId);
    formData.set("productId", productId);
    formData.set("isComplied", status);
    formData.set("userCreatedId", "user123");
    formData.set("violationCause", violation);
    formData.set("file", material);
    formData.set("commercialType", commercialType);
    formData.set("referenceId", referenceId);
    formData.set("mainCategoryId", mainCategoryId);
    formData.set("subCategoryId", subCategoryId);
    formData.set("reason", reason);
    console.log("formdata " + formData);
    setInProgress(true);
    props.uploadMaterial(
      formData,
      percent => setProgressPercent(percent),
      () => {
        toastr.success('Data Successfully Uploaded', 'System');
        handleResetForm();
      }
    );
  };
  const handleResetForm = () => {
    setDuration("");
    setStartRecordTimeStamp("");
    //setDateAired("");
    setStatus(true);
    setScoreQuality("");
    setChannel("");
    setRegion("");
    setCity("");
    setCompanyId("");
    setBrandId("");
    setProductId("");
    setCommercialType("");
    selectedViolation(0);
    setSrc(null);
    setMaterial(null);
    setMaterialId("");
    setInProgress(false);
    setCompany('');
    setBrand('');
    setProduct('');
    setMainCategory('');
    setSubCategory('');
    setTitle('');
    setOriginalDuration('');
  };
  return (
    <React.Fragment>
      <MDBContainer>
        <form
          className="form-group needs-validation"
          onSubmit={handleSubmit}
          noValidate
        >
          <MDBCard>
            <MDBCardHeader><h3 style={{fontWeight: "bold"}}>Matched/Unmatched Materials</h3> <br /></MDBCardHeader>
            
            <MDBCardBody>
              <MDBRow>
                <MDBCol md="6">
                  <label>Material ID</label>
                  <input
                    name={"materialId"}
                    value={materialId}
                    type="text"
                    className="form-control"
                    onBlur={e => handleMaterialId(e)}
                    onChange={e => materialIdOnChangeHandler(e)}
                    required
                  />
                  <div className="invalid-feedback">
                    Please provide material ID
                  </div>
                  <br />
                  <label>Title</label>
                  <input
                    type="text"
                    disabled="disabled"
                    className="form-control"
                    value={title}
                  />
                  <br />
                  <label>Company</label>
                  <input
                    type="text"
                    disabled="disabled"
                    className="form-control"
                    value={company}
                  />
                  <br />
                  <label>Brand</label>
                  <input
                    type="text"
                    disabled="disabled"
                    className="form-control"
                    value={brand}
                  />
                  <br />
                  <label>Product</label>
                  <input
                    type="text"
                    disabled="disabled"
                    className="form-control"
                    value={product}
                  />
                  <br />
                  <label>Main Category</label>
                  <input
                    type="text"
                    disabled="disabled"
                    className="form-control"
                    value={mainCategory}
                  />
                  <br />
                  <label>Sub-Category</label>
                  <input
                    type="text"
                    disabled="disabled"
                    className="form-control"
                    value={subCategory}
                  />
                  <br />
                  <label>Material Duration</label>
                  <input
                    type="text"
                    disabled="disabled"
                    className="form-control"
                    value={originalDuration}
                  />
                  <br />
                </MDBCol>
                <MDBCol md="6">
                  <MDBCol>
                    <label>Play Duration (in secs.)</label>
                    <input
                      name={"duration"}
                      value={duration}
                      type="text"
                      className="form-control"
                      onChange={e => durationOnChangeHandler(e)}
                      required
                    />
                    <div className="invalid-feedback">
                      Please provide duration
                    </div>
                  </MDBCol>
                  <br />
                  <MDBCol>
                    <label>Start Record Timestamp</label>
                    <input
                      name={"startRecordTimeStamp"}
                      value={startRecordTimeStamp}
                      type="text"
                      className="form-control"
                      onChange={e => startRecordTimeStampOnChangeHandler(e)}
                      required
                    />
                    <div className="invalid-feedback">
                      Please provide starting record timestamp
                    </div>
                  </MDBCol>
                  <br />
                  <MDBCol>
                    <label>Score Quality</label>
                    <input
                      name={"scoreQuality"}
                      value={scoreQuality}
                      type="text"
                      className="form-control"
                      onChange={e => scoreQualityOnChangeHandler(e)}
                      required
                    />
                    <div className="invalid-feedback">
                      Please provide score quality
                    </div>
                  </MDBCol>
                  <br />
                  <MDBCol>
                    <label>Channel</label>
                    <input
                      name={"channel"}
                      value={channel}
                      type="text"
                      className="form-control"
                      onChange={e => channelOnChangeHandler(e)}
                      required
                    />
                    <div className="invalid-feedback">
                      Please provide channel
                    </div>
                  </MDBCol>
                  <br />
                  <MDBCol>
                    <label>City</label>
                    <input
                      name={"city"}
                      value={city}
                      type="text"
                      className="form-control"
                      onChange={e => cityOnChangeHandler(e)}
                      required
                    />
                    <div className="invalid-feedback">Please provide City</div>
                  </MDBCol>
                  <br />
                  <MDBCol>
                    <label>Region</label>
                    <input
                      name={"region"}
                      value={region}
                      type="text"
                      className="form-control"
                      onChange={e => regionOnChangeHandler(e)}
                      required
                    />
                    <div className="invalid-feedback">
                      Please provide region
                    </div>
                  </MDBCol>
                  {/* <br />
                  <MDBCol>
                    <label>Date Aired</label>
                    <input
                      name={"dateAired"}
                      value={dateAired}
                      type="date"
                      className="form-control"
                      onChange={e => dateAiredOnChangeHandler(e)}
                      required
                    />
                    <div className="invalid-feedback">
                      Please provide date aired
                    </div>
                  </MDBCol> */}
                  <br />
                  <MDBCol>
                    <label>Violation</label>
                    <Select
                      value={violations}
                      //value={violation}
                      items={violations}
                      onChange={e => violationOnChangeHandler(e)}
                      name={"violationSelect"}
                      key={"violationSelect"}
                      required
                    />
                    <div className="invalid-feedback">
                      Please Select Violation
                    </div>
                  </MDBCol>
                  
                  {violation > 1 ? (
                    
                    <MDBCol>
                      <br />
                      <div className="form-group">
                        <label for="exampleFormControlTextarea2">
                          Reason:
                        </label>
                        <textarea
                          onChange={(e) => reasonOnChangeHandler(e)}
                          className="form-control rounded-1"
                          rows="3"
                        ></textarea>
                      </div>
                    </MDBCol>
                  ) : (
                    console.log("needs no reason " + violation)
                  )}
                  <br />
                  <MDBCol>
                    <label htmlFor="material">Choose File</label>
                    <Dropzone
                      onDrop={handleOnDrop}
                      accept={acceptedFileTypes}
                      multiple={false}
                      maxSize={audioMaxSize}
                    >
                      {({ getRootProps, getInputProps }) => (
                        <Fragment>
                          <div
                            style={{
                              borderColor: "lightgray",
                              borderStyle: "dashed",
                              textAlignment: "center",
                              margin: "5px 15px",
                              height: "120px"
                            }}
                          >
                            <section className={"is-invalid"}>
                              <div
                                {...getRootProps()}
                                style={{ width: "100%" }}
                              >
                                <input
                                  type={"file"}
                                  name={"material"}
                                  key={"material"}
                                  required
                                  {...getInputProps()}
                                />
                                <p
                                  style={{
                                    color: "lightgray",
                                    fontWeight: "bold",
                                    textTransform: "uppercase",
                                    height: "120px",
                                    width: "100%",
                                    textAlign: "center"
                                  }}
                                >
                                  Drag 'n' drop an audio here, or click to
                                  select
                                </p>
                                <div className="invalid-feedback">
                                  Please Attach Audio File
                                </div>
                              </div>
                            </section>
                          </div>
                        </Fragment>
                      )}
                    </Dropzone>
                    <hr />
                    {src && material && (
                      <Fragment>
                        <label>Preview</label>
                        <div style={{ padding: "1rem" }}>
                          <MDBCard>
                            <MDBCardBody>
                              <MDBCardText>
                                Title :{" "}
                                <text style={{ color: "#3283a8" }}>
                                  {material.name}
                                </text>
                              </MDBCardText>
                              <MDBCardText>
                                Type:{" "}
                                <text style={{ color: "#3283a8" }}>
                                  {material.type}
                                </text>
                              </MDBCardText>
                              {src && (
                                <audio
                                  src={src}
                                  controls
                                  style={{ width: "100%" }}
                                ></audio>
                              )}
                            </MDBCardBody>
                          </MDBCard>
                        </div>
                      </Fragment>
                    )}
                  </MDBCol>
                </MDBCol>
              </MDBRow>
            </MDBCardBody>
            <MDBCardFooter style={{ textAlign: "right" }}>
              {isInProgress ? (
                <MDBCol>
                  <label htmlFor="material">Progress</label>
                  <ProgressBar percent={progressPercent} />
                </MDBCol>
              ) : (
                <Fragment>
                  <MDBBtn color="grey" onClick={() => handleResetForm()}>
                    Clear
                  </MDBBtn>
                  <MDBBtn color="light-green" type="submit">
                    Submit
                  </MDBBtn>
                </Fragment>
              )}
            </MDBCardFooter>
          </MDBCard>
        </form>
      </MDBContainer>
      <br />
    </React.Fragment>
  );
};
FormToUpload.propTypes = {
  fetchMaterial: PropTypes.func.isRequired,
  uploadMaterial: PropTypes.func.isRequired
};
const mapStateToProps = state => ({
  fetchedMaterial: state.roles.items
});

export default connect(null, { fetchMaterial, uploadMaterial })(FormToUpload);
