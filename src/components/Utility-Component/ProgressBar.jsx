import React, { Fragment } from "react";

 const ProgressBar = (props) => {
  return (
    <Fragment>
      <div 
        className='progress md-progress'
        style={{height: '20px'}}
      >
        <div
          className='progress-bar'
          role='progressbar'
          style={{width: `${props.percent}%`, height: '20px'}}
          aria-valuenow={`${props.percent}`}
          aria-valuemin='0'
          aria-valuemax='100'
        >
          {Math.round(props.percent)}%
        </div>
      </div>
    </Fragment>
  );
}

export default ProgressBar;