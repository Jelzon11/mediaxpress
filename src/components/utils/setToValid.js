export default (target) => {
  const currentClassName = target.getAttribute('class');
  const newClassName = currentClassName.replace('is-invalid','');
  target.setAttribute('class', newClassName);
}