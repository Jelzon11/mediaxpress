import React, { Component } from "react";
import Routes from "../src/components/Routes";
import TopNavigation from "./components/topNavigation";
import Footer from "./components/Footer";
import "./index.css";
import { Provider } from "react-redux";
import store from "./store";

class App extends Component {
  render() {
    return (
      <Provider store={store}>
      <div>
        <Routes />
      </div>
      </Provider>
    ); 
  }
}

export default App;
