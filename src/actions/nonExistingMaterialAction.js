import { ADD_NON_EXISTING_MATERIAL } from './types';
import axios from 'axios';
const url = process.env.REACT_APP_URL;


export const uploadNonExistingMaterial = (formData, progressCallback, callBack) => dispatch => {
    //console.log(formData);
    axios.request({
      method: 'POST',
      url: url + '/ex/api/unidentifieds/materials',
      //headers: { 'authorization': token },
      data: formData,
      onUploadProgress: (p) => {
        const percent =  p.loaded/p.total;
        progressCallback && progressCallback(percent * 100);
      }
    })
    .then(resData => {
      dispatch({
        type: ADD_NON_EXISTING_MATERIAL,
        payload: resData.data
      })
      callBack && callBack()
    }).catch((err) => {
      console.log('Upload-Action: uploadNonExistingMaterial Error:', err);
    });
  }
  