import { FETCH_DATA, ADD_DATA } from './types';
//import { getToken } from '../utils';
import axios from 'axios';
const url = process.env.REACT_APP_URL;

//const token = 'bearer ' + getToken();

export const fetchMaterial = (materialId, callback) => dispatch => {
  fetch(url + '/ex/api/materials?id=' + materialId.id, {
  
    method: 'GET',
    headers: {
      'content-type': 'application/x-www-form-urlencoded',
      //'authorization': token
    },
    //body: JSON.stringify(materialId)
  })
  .then(res => res.json())
  .then(res =>
    callback(res)
  )
}

export const uploadMaterial = (formData, progressCallback, callBack) => dispatch => {
  //console.log(formData);
  axios.request({
    method: 'POST',
    url: url + '/ex/api/streamrecords',
    //headers: { 'authorization': token },
    data: formData,
    onUploadProgress: (p) => {
      const percent =  p.loaded/p.total;
      progressCallback && progressCallback(percent * 100);
    }
  })
  .then(resData => {
    dispatch({
      type: ADD_DATA,
      payload: resData.data
    })
    callBack && callBack()
  }).catch((err) => {
    console.log('Upload-Action: uploadMaterial Error:', err);
  });
}

